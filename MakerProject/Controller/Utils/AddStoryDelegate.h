//
//  AddStoryDelegate.h
//  MakerProject
//
//  Created by Anca Tache on 21/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Story.h"


@protocol AddStoryDelegate <NSObject>

-(void) addNewStory:(Story *) story;

@end
