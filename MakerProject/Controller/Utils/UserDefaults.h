//
//  UserDefaults.h
//  MakerProject
//
//  Created by Anca Tache on 24/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaults : NSObject

-(void) createNSUserDefaultsWithString:(NSString *)object key:(NSString *)key;

@end
