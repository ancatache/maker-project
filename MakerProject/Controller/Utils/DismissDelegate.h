//
//  DismissDelegate.h
//  MakerProject
//
//  Created by Anca Tache on 22/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol DismissDelegate <NSObject>

-(void) dismissViewController;

@end
