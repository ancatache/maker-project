//
//  ChangeRoot.m
//  MakerProject
//
//  Created by Anca Tache on 24/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "ChangeRoot.h"

@implementation ChangeRoot

-(void) changeRootVC:(NSString *)storyboardName newRootViewController:(UIViewController *)newRootViewController VCidentifier:(NSString*)VCidentifier{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    newRootViewController = [storyboard instantiateViewControllerWithIdentifier:VCidentifier];
    [[UIApplication sharedApplication] keyWindow].rootViewController = newRootViewController;
}

@end
