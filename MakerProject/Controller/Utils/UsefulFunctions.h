//
//  UsefulFunctions.h
//  MakerProject
//
//  Created by Anca Tache on 24/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UsefulFunctions : NSObject

-(void) changeRootVC:(NSString *)storyboardName newRootViewController:(UIViewController *)newRootViewController VCidentifier:(NSString*)VCidentifier;
-(void) createNSUserDefaultsWithString:(NSString *)object key:(NSString *)key;
-(void) createNSUserDefaultsWithObject:(NSObject *)object key:(NSString *)key;
-(NSObject *) readFromNSUserDefaultsObject:(NSString *)key;
-(NSString *) readFromNSUserDefaultsString:(NSString *)key;
-(void) observeNotification:(NSString *)name sel:(SEL)selector;
-(void) postNotification:(NSObject *)object key:(NSString *)key notificationName:(NSString *)notificationName;
-(void) registerNibTableView:(NSString *)nibName identifier:(NSString *)identifier tableView:(UITableView *)tableView;
-(void) registerNibCollectionView:(NSString *)nibName identifier:(NSString *)identifier collectionView:(UICollectionView *)collectionView;
-(void) deleteUserDefaults;


@end
