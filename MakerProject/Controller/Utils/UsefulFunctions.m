//
//  UsefulFunctions.m
//  MakerProject
//
//  Created by Anca Tache on 24/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "UsefulFunctions.h"

@implementation UsefulFunctions

-(void) changeRootVC:(NSString *)storyboardName newRootViewController:(UIViewController *)newRootViewController VCidentifier:(NSString*)VCidentifier {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    newRootViewController = [storyboard instantiateViewControllerWithIdentifier:VCidentifier];
    [[UIApplication sharedApplication] keyWindow].rootViewController = newRootViewController;
}

-(void) createNSUserDefaultsWithString:(NSString *)object key:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) createNSUserDefaultsWithObject:(NSObject *)object key:(NSString *)key {
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSObject *) readFromNSUserDefaultsObject:(NSString *)key {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    return [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
}

-(NSString *) readFromNSUserDefaultsString:(NSString *)key {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults stringForKey:key];
}

-(void) observeNotification:(NSString *)name sel:(SEL)selector {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:selector
                                                 name:name
                                               object:nil];
}

-(void) postNotification:(NSObject *)object key:(NSString *)key notificationName:(NSString *)notificationName {
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:object forKey:key];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:userInfo];
}

-(void) registerNibTableView:(NSString *)nibName identifier:(NSString *)identifier tableView:(UITableView *)tableView {
    
    UINib *cellNib = [UINib nibWithNibName:nibName bundle:nil];
    [tableView registerNib:cellNib forCellReuseIdentifier:identifier];
    
}

-(void) registerNibCollectionView:(NSString *)nibName identifier:(NSString *)identifier collectionView:(UICollectionView *)collectionView {
    
    UINib *cellNib = [UINib nibWithNibName:nibName bundle:nil];
    [collectionView registerNib:cellNib forCellWithReuseIdentifier:identifier];
    
}

-(void) deleteUserDefaults {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"newstory"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




@end
