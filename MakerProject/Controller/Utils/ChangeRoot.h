//
//  ChangeRoot.h
//  MakerProject
//
//  Created by Anca Tache on 24/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ChangeRoot : NSObject

-(void) changeRootVC:(NSString *)storyboardName newRootViewController:(UIViewController *)newRootViewController VCidentifier:(NSString*)VCidentifier;

@end
