//
//  UserDefaults.m
//  MakerProject
//
//  Created by Anca Tache on 24/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "UserDefaults.h"

@implementation UserDefaults

-(void) createNSUserDefaultsWithString:(NSString *)object key:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
