//
//  ModifyNameDelegate.h
//  MakerProject
//
//  Created by Anca Tache on 21/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ModifyNameDelegate <NSObject>

-(void) modifyName:(NSString *) name;

@end


