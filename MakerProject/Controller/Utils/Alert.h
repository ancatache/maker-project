//
//  ShowAlert.h
//  MakerProject
//
//  Created by Anca on 4/5/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Alert : UIViewController

-(void)showAlert:(NSString *)message second:(NSString *)title;

@end
