//
//  VentureTableViewCell.m
//  MakerProject
//
//  Created by Carmen Popa on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "VentureTableViewCell.h"

@implementation VentureTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
