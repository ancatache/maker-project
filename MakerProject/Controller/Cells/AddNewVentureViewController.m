//
//  AddNewVentureViewController.m
//  MakerProject
//
//  Created by Carmen Popa on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "AddNewVentureViewController.h"

@interface AddNewVentureViewController ()

@end

@implementation AddNewVentureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.ventureTitleTextField resignFirstResponder];
}

- (IBAction)createNewVenture:(id)sender {
    [self.delegate addNewVenture:self.ventureTitleTextField.text];
    self.ventureTitleTextField.text = @"";
    [self.ventureTitleTextField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:true];
    
}

@end
