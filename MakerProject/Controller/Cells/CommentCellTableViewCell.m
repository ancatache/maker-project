//
//  CommentCellTableViewCell.m
//  MakerProject
//
//  Created by Carmen Popa on 21/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "CommentCellTableViewCell.h"

@implementation CommentCellTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.indexPath = [NSIndexPath new];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickOnResolve:(id)sender {
    if([self.dismissButton.titleLabel.text isEqualToString:@"Dismiss"]){
        if ([self.resolveButton.titleLabel.text  isEqualToString:@"Resolve"]){
            [self.checkedResolvedImageView setImage:[UIImage imageNamed: @"checked-square"]];
            [self.resolveButton setTitle:@"Resolved" forState:UIControlStateNormal];
        }
        else{
            [self.checkedResolvedImageView setImage:[UIImage imageNamed: @"square"]];
            [self.resolveButton setTitle:@"Resolve" forState:UIControlStateNormal];
        }
    }
}

- (IBAction)clickOnDismiss:(id)sender {
    if([self.resolveButton.titleLabel.text isEqualToString:@"Resolve"]){
        if([self.dismissButton.titleLabel.text isEqualToString:@"Dismiss"]){
            [self.checkedDismissedImageView setImage:[UIImage imageNamed: @"checked-square"]];
            [self.dismissButton setTitle:@"Dismissed" forState:UIControlStateNormal];
        }
        else{
            [self.checkedDismissedImageView setImage:[UIImage imageNamed: @"square"]];
            [self.dismissButton setTitle:@"Dismiss" forState:UIControlStateNormal];
        }
    }
}



@end
