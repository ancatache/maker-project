//
//  CommentCellTableViewCell.h
//  MakerProject
//
//  Created by Carmen Popa on 21/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *resolveButton;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UIImageView *checkedResolvedImageView;
@property (weak, nonatomic) IBOutlet UIImageView *checkedDismissedImageView;
@property NSIndexPath *indexPath;
@end
