//
//  VentureTableViewCell.h
//  MakerProject
//
//  Created by Carmen Popa on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VentureTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ventureTitleLabel;

@end
