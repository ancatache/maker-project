//
//  AddNewStoryViewController.m
//  MakerProject
//
//  Created by Anca on 3/9/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "AddNewStoryViewController.h"
#import "PhotoCell.h"
#import "LinkCell.h"
#import "AddStoryViewController.h"


@interface AddNewStoryViewController ()

@property (strong, nonatomic) IBOutletCollection(UIStackView) NSArray *array;


@end

NSMutableArray *images;
NSMutableArray *links;



@implementation AddNewStoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _usefulFunctions = [[UsefulFunctions alloc] init];
   
    // Do any additional setup after loading the view.
    
    self.imagesCollectionView.delegate = self;
    self.imagesCollectionView.dataSource = self;
    
    self.linksTableView.delegate = self;
    self.linksTableView.dataSource = self;
  
    images = [[NSMutableArray alloc] init];
    links = [[NSMutableArray alloc] init];
    
    NSString *VCname = [_usefulFunctions readFromNSUserDefaultsString:@"fromWhat"];
    if([VCname isEqualToString:@"stories"]) {
        Story *story = (Story *)[_usefulFunctions readFromNSUserDefaultsObject:@"newstorycell"];
        _contentLabel.text = story.content;
        _dateLabel.text = story.date;
        [_usefulFunctions deleteUserDefaults];
    }
    else
    if([VCname isEqualToString:@"addNew"]) {
        Story *story = (Story *)[_usefulFunctions readFromNSUserDefaultsObject:@"addnewstory"];
        _contentLabel.text = story.content;
        _dateLabel.text = story.date;
        [_usefulFunctions deleteUserDefaults];
    }
    
    NSString *value = [_usefulFunctions readFromNSUserDefaultsString:@"ventureNameFromCell"];
    _ventureName.title = value;
    
    [_usefulFunctions registerNibCollectionView:@"PhotoCell" identifier:@"Cell" collectionView:self.imagesCollectionView];
    [_usefulFunctions registerNibTableView:@"LinkCell" identifier:@"linkCell" tableView:self.linksTableView];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
}

- (void)addLink:(NSString *)link {
    
    [links addObject:link];
    [_linksTableView reloadData];
}
- (IBAction)doneButton:(UIBarButtonItem *)sender {
    
    Story *story = [[Story alloc] init];
    [story init:_dateLabel.text content:_contentLabel.text];
    [self.delegate addNewStory:story];

    [_usefulFunctions deleteUserDefaults];
    [self.dismissdelegate dismissViewController];

}


- (IBAction)uploadImage:(UIButton *)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSData *pngData = UIImagePNGRepresentation(chosenImage);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSString *name = [NSString stringWithFormat:@"%lung.png", (unsigned long)images.count];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:name]; //Add the file name
    [pngData writeToFile:filePath atomically:YES];
    [images addObject:name];
    
    [_imagesCollectionView reloadData];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)addNewLink:(UIButton *)sender {

   UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Add New Link"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [links addObject:alert.textFields[0].text ];
                                    [_linksTableView reloadData];
                                    [alert dismissViewControllerAnimated:YES completion:nil];

                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"New Link";
        
    }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [images count];
}

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    PhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSData *pngData = [NSData dataWithContentsOfFile:[self documentsPathForFileName:[images objectAtIndex:indexPath.row]]];
    UIImage *image = [UIImage imageWithData:pngData];
    cell.imageUploaded.image = image;
    
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [links count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"linkCell";
    
    LinkCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[LinkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.linkName.text = [links objectAtIndex:indexPath.row];
    cell.linkCellDelegate = self;
    
    return cell;
    
}

- (IBAction)showImages:(UIButton *)sender {
    self.imagesView.hidden = !self.imagesView.hidden;
    self.linksView.hidden = true;
    self.sendView.hidden = true;
    self.commentsView.hidden = true;
}
- (IBAction)showLinks:(UIButton *)sender {
    self.linksView.hidden = !self.linksView.hidden;
    self.imagesView.hidden = true;
    self.sendView.hidden = true;
    self.commentsView.hidden = true;

}
- (IBAction)showSend:(UIButton *)sender {
    self.sendView.hidden = !self.sendView.hidden;
    self.linksView.hidden = true;
    self.imagesView.hidden = true;
    self.commentsView.hidden = true;
    
}

- (IBAction)showComments:(UIButton *)sender {
    self.commentsView.hidden = !self.commentsView.hidden;
    self.linksView.hidden = true;
    self.sendView.hidden = true;
    self.imagesView.hidden = true;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editStory:(UIButton *)sender {
    
    Story *story = [[Story alloc] init];
    [story init:_dateLabel.text content:_contentLabel.text];
    [_usefulFunctions createNSUserDefaultsWithObject:story key:@"newstory"];
    
  
    [self dismissViewControllerAnimated:YES completion:nil];
  
 
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
