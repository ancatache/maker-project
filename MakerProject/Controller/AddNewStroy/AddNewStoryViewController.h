//
//  AddNewStoryViewController.
//  MakerProject
//
//  Created by Anca on 3/9/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LinkCellDelegate.h"
#import "AddStoryDelegate.h"
#import "DismissDelegate.h"
#import "UsefulFunctions.h"


@interface AddNewStoryViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, LinkCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *imagesCollectionView;
@property (weak, nonatomic) IBOutlet UIView *imagesView;
@property (weak, nonatomic) IBOutlet UIButton *uploadImagesButton;
@property (weak, nonatomic) IBOutlet UIButton *showImagesButton;
@property (weak, nonatomic) IBOutlet UIButton *showLinksButton;
@property (weak, nonatomic) IBOutlet UIView *linksView;
@property (weak, nonatomic) IBOutlet UITableView *linksTableView;
@property (weak, nonatomic) IBOutlet UIButton *addNewLinkButton;
@property (weak, nonatomic) IBOutlet UIView *sendView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *showSendButton;
@property (weak, nonatomic) IBOutlet UIButton *showComments;
@property (weak, nonatomic) IBOutlet UIView *commentsView;
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UINavigationItem *ventureName;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property(weak, nonatomic) id <AddStoryDelegate> delegate;
@property(weak, nonatomic) id <DismissDelegate> dismissdelegate;
@property(strong, nonatomic) UsefulFunctions *usefulFunctions;


@end
