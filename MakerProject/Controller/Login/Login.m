//
//  Login.m
//  MakerProject
//
//  Created by Anca on 2/27/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "Login.h"
#import <TwitterKit/TwitterKit.h>
#import <AFHTTPSessionManager.h>
#import "MyVenturesViewController.h"
#import "BaseAPI.h"




@interface Login ()

@end

@implementation Login

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            // Callback for login success or failure. The TWTRSession
            // is also available on the [Twitter sharedInstance]
            // singleton.
            //
            // Here we pop an alert just to give an example of how
            // to read Twitter user info out of a TWTRSession.
            //
            // TODO: Remove alert and use the TWTRSession's userID
            // with your app's user model
            
            NSDictionary *head = @{
                                   @"Content-Type":@"application/json"
                                   };
            
            NSDictionary *param = @{
                                    @"access_token": [session authToken],
                                    @"access_secret": [session authTokenSecret],
                                    @"provider": @"Twitter"
                                    };
            
            
            NSString *URL = @"http://api.staging.ideatify.pro/api/v1/session";
            
            
            void (^successBlock)(NSURLSessionTask *, id ) = ^(NSURLSessionTask *task, id responseObject) {
                // NSLog(@"JSON: %@", responseObject);
                
                UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                
                MyVenturesViewController *newRootViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"myVentures"];
                
                [[UIApplication sharedApplication] keyWindow].rootViewController = newRootViewController;
                
                NSString *message = [NSString stringWithFormat:@"@%@ logged in!",
                                     [session userName]];
                
            //    Alert *alert = [[Alert alloc] init];
            //    [alert showAlert:message second:@"Logged In!"];
                
                
            };
            
            void (^failureBlock)(NSURLSessionTask *, NSError *) = ^(NSURLSessionTask *task, NSError *error) {
                NSLog(@"Error: %@", error);
            };
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            manager.responseSerializer = [AFJSONResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",nil];
            [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            
            
            [manager POST:URL parameters:param progress:nil success:successBlock failure:failureBlock];

            
          
        } else {
            NSLog(@"Login Twitter error: %@", [error localizedDescription]);
        }
    }];
    
    // TODO: Change where the log in button is positioned in your view
   // CGPoint point;
   // point.x = self.view.center.x;
  //  point.y = self.view.bounds.size.height - 100;
  
   // logInButton.center = point;
    
    logInButton.center = self.view.center;
    
    [self.view addSubview:logInButton];



    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
