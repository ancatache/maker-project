//
//  Header.h
//  MakerProject
//
//  Created by Alexandru Matei on 25/04/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol LinkCellDelegate <NSObject>

-(void) addLink:(NSString *)link;

@end
