//
//  AddStoryViewController.h
//  MakerProject
//
//  Created by Anca Tache on 20/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Story.h"
#import "AddStoryDelegate.h"
#import "DismissDelegate.h"
#import "UsefulFunctions.h"




@interface AddStoryViewController : UIViewController <DismissDelegate>

@property (weak, nonatomic) IBOutlet UITextView *storyTextView;
@property (weak, nonatomic) IBOutlet UILabel *ventureNameLabel;
@property (strong, nonatomic) UsefulFunctions *usefulFunctions;

@property(weak, nonatomic) id <AddStoryDelegate> delegate;
@property(weak, nonatomic) id <DismissDelegate> dismissdelegate;

@end
