//
//  StoriesViewController.h
//  MakerProject
//
//  Created by Anca on 4/6/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DismissDelegate.h"
#import "AddStoryDelegate.h"
#import "UsefulFunctions.h"



@interface StoriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, AddStoryDelegate, DismissDelegate>

@property (weak, nonatomic) IBOutlet UILabel *ventureNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *storiesTableView;
@property (strong, nonatomic) UsefulFunctions *usefulFunctions;





@end
