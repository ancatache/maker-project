//
//  AddStoryViewController.m
//  MakerProject
//
//  Created by Anca Tache on 20/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "AddStoryViewController.h"
#import "StoriesViewController.h"
#import "AddNewStoryViewController.h"


@interface AddStoryViewController ()

@end



@implementation AddStoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _usefulFunctions = [[UsefulFunctions alloc] init];
    
    NSString *savedValue = [_usefulFunctions readFromNSUserDefaultsString:@"ventureNameFromCell"];
    _ventureNameLabel.text = savedValue;
    
    Story *value = (Story *)[_usefulFunctions readFromNSUserDefaultsObject:@"newstory"];
    _storyTextView.text = value.content;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[AddNewStoryViewController class]]) {
        AddNewStoryViewController *addStory = segue.destinationViewController;
        addStory.delegate = self.delegate;
        addStory.dismissdelegate = self;
    }
}

-(void) dismissViewController {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    [self.dismissdelegate dismissViewController];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.storyTextView resignFirstResponder];
}

- (IBAction)addStoryAction:(id)sender {
    
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString = [@"added on " stringByAppendingString:[dateFormat stringFromDate:today]];
    
    Story *story = [[Story alloc] init];
    [story init:dateString content:_storyTextView.text];
    _storyTextView.text = @"";
    [_storyTextView resignFirstResponder];
    
    [_usefulFunctions createNSUserDefaultsWithString:@"addNew" key:@"fromWhat"];
    [_usefulFunctions createNSUserDefaultsWithObject:story key:@"addnewstory"];
    
}



- (void) receiveTestNotification:(NSNotification *) notification
{
 
    NSDictionary *userInfo = notification.userInfo;
    NSString *myObject = [userInfo objectForKey:@"nametwo"];
    _ventureNameLabel.text = myObject;
    NSString *myObject2 = [userInfo objectForKey:@"editnotification"];
    if([myObject2 isEqualToString:@"edit"])
        [_usefulFunctions createNSUserDefaultsWithString:@"edit" key:@"ifedit"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
