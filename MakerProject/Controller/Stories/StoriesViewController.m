//
//  StoriesViewController.m
//  MakerProject
//
//  Created by Anca on 4/6/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "StoriesViewController.h"
#import "AddNewStoryViewController.h"
#import "StoryTableViewCell.h"
#import "AddStoryViewController.h"
#import <AFHTTPSessionManager.h>
#import "BaseAPI.h"

@interface StoriesViewController ()

@end

NSMutableArray *stories;

@implementation StoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _usefulFunctions = [[UsefulFunctions alloc] init];
    stories = [[NSMutableArray alloc] init];
    
    self.storiesTableView.delegate = self;
    self.storiesTableView.dataSource = self;
    
    [_usefulFunctions registerNibTableView:@"StoryTableViewCell" identifier:@"storyCell" tableView:self.storiesTableView];
    
    NSString *savedValue = [_usefulFunctions readFromNSUserDefaultsString:@"ventureNameFromCell"];
    _ventureNameLabel.text = savedValue;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.storiesTableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (IBAction)addStoryAction:(id)sender {
    
    
   
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [stories count];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.destinationViewController isKindOfClass:[AddStoryViewController class]]) {
        AddStoryViewController *addStory = segue.destinationViewController;
        addStory.delegate = self;
        addStory.dismissdelegate = self;
    }
    
    if ([segue.destinationViewController isKindOfClass:[AddNewStoryViewController class]]) {
        AddNewStoryViewController *addStory = segue.destinationViewController;
        addStory.dismissdelegate = self;
    }
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"storyCell";
    
    StoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

    if (cell == nil) {
        cell = [[StoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    Story *story = [stories objectAtIndex:indexPath.row];
    cell.contentStory.text = story.content;
    cell.dateStory.text = story.date;
    
    return cell;
}

-(void) dismissViewController {
    
   [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) addNewStory:(Story *)story {
    
    [stories addObject:story];
    [_storiesTableView reloadData];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [stories removeObjectAtIndex:indexPath.row];
    [self.storiesTableView reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    Story *story = [Story alloc];
    [story init:cell.dateStory.text content:cell.contentStory.text];
    [_usefulFunctions createNSUserDefaultsWithObject:story key:@"newstorycell"];
    [_usefulFunctions createNSUserDefaultsWithString:@"stories" key:@"fromWhat"];
    
    UIStoryboard *storiesStoryBoard = [UIStoryboard storyboardWithName:@"AddNewStory" bundle:nil];
    AddNewStoryViewController *addNewStoryViewController = [storiesStoryBoard instantiateViewControllerWithIdentifier:@"addNewStory"];
    [self presentViewController:addNewStoryViewController animated:YES completion:nil];
    addNewStoryViewController.dismissdelegate = self;
    
}


    



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
