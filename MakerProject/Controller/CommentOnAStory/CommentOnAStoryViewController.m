//
//  CommentOnAStoryViewController.m
//  MakerProject
//
//  Created by Carmen Popa on 09/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "CommentOnAStoryViewController.h"
#import "CommentCellTableViewCell.h"
#import "Comment.h"

@interface CommentOnAStoryViewController ()

@end

NSMutableArray *commentTableData;


@implementation CommentOnAStoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    commentTableData = [NSMutableArray new];

    self.commentsTable.delegate = self;
    self.commentsTable.dataSource = self;
    self.commentTextView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(keyboardDidShow:)
                                            name:UIKeyboardDidShowNotification
                                            object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(keyboardDidHide:)
                                            name:UIKeyboardDidHideNotification
                                            object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [commentTableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CommentCellTableViewCell class])];
    
    if (cell == nil) {
        cell = [[CommentCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([CommentCellTableViewCell class])];
    }
    Comment *comment = [commentTableData objectAtIndex:indexPath.row];
    cell.commentLabel.text = comment.comment;
    cell.titleLabel.text = comment.title;
    cell.indexPath = indexPath;
    
    switch (comment.cellState) {
        case Resolved:
            
            break;
        case Dismissed:
            
            break;
        default: //None
            
            break;
    }
    
    return cell;
}

- (IBAction)addComment:(id)sender {
    if([self.commentTextView.text length] != 0){
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
        NSDate *date = [NSDate date];
        NSString *formattedDateString = [dateFormatter stringFromDate:date];
        
        
        Comment *comment = [Comment new];
        
        comment.title       = formattedDateString;
        comment.comment     = _commentTextView.text;
        comment.cellState   = None;
        
        self.commentTextView.text = @"";
        [self.commentTextView resignFirstResponder];
        
        [commentTableData addObject:comment];
        [self.commentsTable reloadData];

    }
}

//Anca, this methods deletes a row, no worries.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [commentTableData removeObjectAtIndex:indexPath.row];
//    [dateAndTime removeObjectAtIndex:indexPath.row];
//    [selectedCommentsData removeObjectAtIndex:indexPath.row];
    [self.commentsTable reloadData];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.commentTextView resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextView *)textView {
    [self.commentTextView resignFirstResponder];
    return NO;
}

- (void)keyboardDidShow: (NSNotification *) notif{
    self.bottomConstraint.constant = 300;
}

- (void)keyboardDidHide: (NSNotification *) notif{
    self.bottomConstraint.constant = 35;
}

//
@end
