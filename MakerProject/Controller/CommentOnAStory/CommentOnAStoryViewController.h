//
//  CommentOnAStoryViewController.h
//  MakerProject
//
//  Created by Carmen Popa on 09/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentOnAStoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *commentsTable;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
- (BOOL)textFieldShouldReturn:(UITextView *)textView;
@end
