//
//  AddNewVentureViewController.h
//  MakerProject
//
//  Created by Carmen Popa on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DismissDelegate.h"
#import "UsefulFunctions.h"

@protocol AddNewVenture <NSObject>

-(void) addNewVenture:(NSString *) ventureTitle;

@end


@interface AddNewVentureViewController : UIViewController 

@property (weak, nonatomic) IBOutlet UITextField *ventureTitleTextField;
@property (weak, nonatomic) IBOutlet UIButton *createButton;

@property(weak, nonatomic) id <AddNewVenture> delegate;
@property(strong, nonatomic) UsefulFunctions *usefulFunctions;


@end
