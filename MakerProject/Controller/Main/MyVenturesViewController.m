//
//  MyVenturesViewController.m
//  MakerProject
//
//  Created by Carmen Popa on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "MyVenturesViewController.h"
#import "VentureTableViewCell.h"
#import "AddNewVentureViewController.h"
#import <AFHTTPSessionManager.h>
#import "BaseAPI.h"
#import "StoriesViewController.h"

@interface MyVenturesViewController ()

@end

NSMutableArray *tableData;

@implementation MyVenturesViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    tableData = [NSMutableArray array];
    _usefulFunctions = [[UsefulFunctions alloc] init];
    
    self.ventureTableView.delegate = self;
    self.ventureTableView.dataSource = self;
    
    NSString *URL = @"http://api.staging.ideatify.pro/api/v1/ventures";
    
    BaseAPI *baseAPI = [[BaseAPI alloc] init];
    
    void (^successBlock)(NSURLSessionTask *, id ) = ^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    };
    
    void (^failureBlock)(NSURLSessionTask *, NSError *) = ^(NSURLSessionTask *task, NSError *error) {
        NSLog(@"Error: %@", error);
    };
    
    
    [baseAPI performRequest:nil second:nil third:@"GET" fourth:URL fifth:successBlock sixth:failureBlock];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.ventureTableView reloadData];
    if ([tableData count]==0) self.statusLabel.text = @"No ventures yet";
        else self.statusLabel.text = @"";
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[AddNewVentureViewController class]]) {
        AddNewVentureViewController *addNewVenture = segue.destinationViewController;
        addNewVenture.delegate = self;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VentureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VentureTableViewCell class])];
    
    if (cell == nil) {
        cell = [[VentureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NSStringFromClass([VentureTableViewCell class])];
    }
    
    cell.ventureTitleLabel.text = [tableData objectAtIndex:indexPath.row];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableData removeObjectAtIndex:indexPath.row];
    [self.ventureTableView reloadData];
}

- (void) addNewVenture:(NSString*) ventureTitle{
    [tableData addObject:ventureTitle];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        
    VentureTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [_usefulFunctions createNSUserDefaultsWithString:cell.ventureTitleLabel.text key:@"ventureNameFromCell"];
    
    StoriesViewController *newRootViewController = [StoriesViewController alloc];
    [_usefulFunctions changeRootVC:@"Stories" newRootViewController:newRootViewController VCidentifier:@"venture" ];
}


@end
