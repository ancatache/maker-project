//
//  MyVenturesViewController.h
//  MakerProject
//
//  Created by Carmen Popa on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddNewVentureViewController.h"
#import "ModifyNameDelegate.h"
#import "UsefulFunctions.h"

@interface MyVenturesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, AddNewVenture>


@property (weak, nonatomic) IBOutlet UITableView *ventureTableView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (strong, nonatomic) UsefulFunctions *usefulFunctions;



@end
