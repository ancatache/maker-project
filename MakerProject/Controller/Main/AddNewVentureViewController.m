//
//  AddNewVentureViewController.m
//  MakerProject
//
//  Created by Carmen Popa on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "AddNewVentureViewController.h"
#import "MyVenturesViewController.h"
#import <AFHTTPSessionManager.h>
#import "BaseAPI.h"

@interface AddNewVentureViewController ()

@end

@implementation AddNewVentureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.ventureTitleTextField resignFirstResponder];
}


- (IBAction)createNewVenture:(id)sender {
    
    [self.delegate addNewVenture:self.ventureTitleTextField.text];
    self.ventureTitleTextField.text = @"";
    [self.ventureTitleTextField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:true];
    
    
    NSDictionary *head = @{
                           @"Content-Type":@"application/json",
                           @"Authorization":@"Token token=&#34;authorization_token&#34;"
                           };
    
    NSDictionary *param = @{
                            @"name": self.ventureTitleTextField.text,
                            @"description": @" "
                            
                            };
    
    NSString *URL = @"http://api.staging.ideatify.pro/api/v1/ventures";
    
    BaseAPI *baseAPI = [[BaseAPI alloc] init];
    
    void (^successBlock)(NSURLSessionTask *, id ) = ^(NSURLSessionTask *task, id responseObject) {
         NSLog(@"JSON: %@", responseObject);
    };
    
    void (^failureBlock)(NSURLSessionTask *, NSError *) = ^(NSURLSessionTask *task, NSError *error) {
        NSLog(@"Error: %@", error);
    };
    
    
    [baseAPI performRequest:head second:param third:@"POST" fourth:URL fifth:successBlock sixth:failureBlock];
    

    
}

@end
