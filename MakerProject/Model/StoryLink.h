//
//  StoryLink.h
//  MakerProject
//
//  Created by Diana Ghinea on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoryLink : NSObject

@property NSString *name;
@property NSString *href;

@end
