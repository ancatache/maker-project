//
//  Story.m
//  MakerProject
//
//  Created by Diana Ghinea on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "Story.h"

@implementation Story

- (void) init:(NSString *)date content:(NSString *)content {
    
    self.date = date;
    self.content = content;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.content forKey:@"content"];
    [encoder encodeObject:self.date forKey:@"date"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.content = [decoder decodeObjectForKey:@"content"];
        self.date = [decoder decodeObjectForKey:@"date"];
    }
    return self;
}

@end
