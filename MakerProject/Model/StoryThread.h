//
//  StoryThread.h
//  MakerProject
//
//  Created by Diana Ghinea on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoryComment.h"

@interface StoryThread : NSObject
@property NSMutableArray<StoryComment *> *comments;
@end
