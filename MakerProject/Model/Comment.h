//
//  Comment.h
//  MakerProject
//
//  Created by Carmen Popa on 04/04/2017.
//  Copyright © 2017 Dev. All rights reserved.
//


#import <Foundation/Foundation.h>

typedef enum {
    Dismissed,
    Resolved,
    None
} CellState;

@interface Comment : NSObject

@property CellState cellState;
@property NSString *comment;
@property NSString *title;
@end
