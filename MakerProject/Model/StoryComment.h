//
//  StoryComment.h
//  MakerProject
//
//  Created by Diana Ghinea on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoryComment : NSObject

@property NSString *username;
@property NSString *text;
@property NSDate *date;


@end
