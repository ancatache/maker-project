//
//  Story.h
//  MakerProject
//
//  Created by Diana Ghinea on 07/03/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoryLink.h"
#import "StoryComment.h"

@interface Story : NSObject

- (void) init:(NSString *)date content:(NSString *)content;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;

@property NSString *content;
@property NSString *date;
@end
