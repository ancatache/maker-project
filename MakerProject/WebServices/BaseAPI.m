//
//  BaseAPIViewController.m
//  MakerProject
//
//  Created by Anca on 3/30/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "BaseAPI.h"
#import <AFHTTPSessionManager.h>


@interface BaseAPI ()

@end

@implementation BaseAPI



-(void) performRequest:(NSDictionary *)headers
                second:(NSDictionary *)parameters
                 third:(NSString *)typeRequest
                fourth:(NSString *)URL
                 fifth:(void (^)(NSURLSessionTask *task, id responseObject))successBlock
                 sixth:(void(^)(NSURLSessionTask *operation, NSError *error))failureBlock {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",nil];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    if([typeRequest isEqualToString:@"POST"]) {
        
        [manager POST:URL parameters:parameters progress:nil success:successBlock failure:failureBlock];
        
        }
    
    if([typeRequest isEqualToString:@"GET"]) {
        
        [manager GET:URL parameters:parameters progress:nil success:successBlock failure:failureBlock];

        }             
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
