//
//  BaseAPIViewController.h
//  MakerProject
//
//  Created by Anca on 3/30/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseAPI : NSObject 


@property NSString *baseURL;

-(void) performRequest:(NSDictionary *)headers second:(NSDictionary *)parameters third:(NSString *)typeRequest fourth:(NSString *)URL fifth:(void (^)(NSURLSessionTask *task, id responseObject))successBlock  sixth:(void(^)(NSURLSessionTask *operation, NSError *error))failureBlock;

@end
