//
//  StoryCellTableViewCell.h
//  MakerProject
//
//  Created by Anca Tache on 20/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *contentStory;
@property (weak, nonatomic) IBOutlet UILabel *dateStory;

@end
