//
//  LinkCell.h
//  MakerProject
//
//  Created by Anca on 4/8/17.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LinkCellDelegate.h"

@interface LinkCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *linkName;
@property (weak, nonatomic) id<LinkCellDelegate> linkCellDelegate;
@end
