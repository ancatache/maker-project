//
//  StoryTableViewCell.m
//  MakerProject
//
//  Created by Anca Tache on 20/06/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import "StoryTableViewCell.h"

@implementation StoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
