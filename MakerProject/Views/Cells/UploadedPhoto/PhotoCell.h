//
//  PhotoViewCell.h
//  MakerProject
//
//  Created by Alexandru Matei on 06/04/2017.
//  Copyright © 2017 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageUploaded;
@property NSIndexPath *indexPath;

@end
